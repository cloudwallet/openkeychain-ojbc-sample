//
//  KeyChainTestTests.m
//  KeyChainTestTests
//
//  Created by Won Beom Kim on 2016. 2. 17..
//  Copyright © 2016년 Blocko. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OpenKeyChainLib.h>

@interface KeyChainTestTests : XCTestCase

@end

@implementation KeyChainTestTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testKeyOperations {
    OpenKeyChainLib *okcL = [[OpenKeyChainLib alloc] init];
    XCTAssertNotNil(okcL, @"");
    XCTAssertNotNil(okcL.createPrivateKey, @"");
    NSString *privateKey = okcL.createPrivateKey;
    NSLog(@"%@", privateKey);
    NSLog(@"%@", [okcL deriveAddress:privateKey]);
}

- (void)testChallengeResponse {
    NSTimeInterval interval = 60 * 60 * 10000;
    OpenKeyChainLib *okcL = [[OpenKeyChainLib alloc] initWithTimeoutThreshold:interval];
    XCTAssertNotNil(okcL, @"");
    XCTAssertFalse([okcL checkChallenge:@"testdata" authorityAddress:@"test"], @"");
    XCTAssertTrue([okcL checkChallenge:@"{\"signature\":\"IMbDOLjqIvwT3uB3snFElKmtUxSgAPLADDAeFQuqYIzSOhj68mONbeHGfBgQZmnx53C6keYunl85x1L5QKppRkM=\",\"context\":\"testcontext\",\"timestamp\":\"2016-02-17T01:23:52.877+09:00\"}" authorityAddress:@"1Da2TJtMD855uPTer6LipMVwN2GanQqoHn"], @"");
    XCTAssertFalse([okcL checkChallenge:@"{\"signature\":\"IMbDOLjqIvwT3uB3snFElKmtUxSgAPLADDAeFQuqYIzSOhj68mONbeHGfBgQZmnx53C6keYunl85x1L5QKppRkM=\",\"context\":\"testcontext\",\"timestamp\":\"2016-02-17T01:23:52.877+09:10\"}" authorityAddress:@"1Da2TJtMD855uPTer6LipMVwN2GanQqoHn"], @"");
    
    NSString* privateKey = @"L3pxpb3W7Q8bg1VTeN4j73u1u2tnCYwT5gZDsAWgKDzZQkUULPtU";
    NSString* response = [okcL createResponse:@"{\"signature\":\"Hy+oBiRNO7BK4aSknGSFqeJFAg4Ou4VV18I0npJQ4rWKcp2hnl5RZ2gtVohJ/aazDH3/15dvKrUVVOLCUaq4Yrk=\",\"context\":\"testcontext\",\"timestamp\":\"2016-02-17T05:08:19.843+09:00\"}\r\n" privateKey: privateKey];
    XCTAssertNotNil(response, @"");
    NSLog(@"%@", response);
    
}

@end
